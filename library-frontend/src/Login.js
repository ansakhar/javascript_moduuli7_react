import React from "react";
import Banner from "./Banner";
import {useHistory} from "react-router-dom";

export default function Login(){
    //tarkista palvelimelta tunnus ja salasana
    // storeen on kirjautunut
    const history = useHistory();
  
    const handleLogin = () =>{
        history.push("/library");
    };
  
    return (
        <div className="login">
            <Banner/>
            <form>
                <table>
                    <tbody>
                        <tr>
                            <td>Name:</td>
                            <td><input type="text" className="loginInput"/></td>
                        </tr>
                        <tr>
                            <td>Password:</td>
                            <td><input type="password" className="loginInput"/></td>
                        </tr>
                        <tr>
                            <td><input type="button" className = "btn btn-primary logout-button" value="Login" onClick={() => handleLogin()}/></td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
    );
}