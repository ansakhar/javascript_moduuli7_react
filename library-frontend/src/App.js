import "./App.css";
import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import Login from "./Login";
import BooksFormAndList from "./BooksFormAndList";


function App() {
    return (
        <Router>
            <Switch>
                <Route exact path="/"><Login /></Route>
                <Route path="/library"><BooksFormAndList /></Route>
            </Switch>
        </Router> 
    );
}

export default App;
