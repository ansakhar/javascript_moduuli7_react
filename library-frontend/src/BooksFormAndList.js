import React, {useState} from "react";
import { books_array } from "./books_array";
import Book from "./Book";
import Banner from "./Banner";
import {useHistory} from "react-router-dom";

import { v4 as uuidv4 } from "uuid";

export default function BooksFormAndList() {
    const [books, setBooks] = useState(books_array); 
    const [bookName, setBookName] = useState(""); 
    const [bookAuthor, setBookAuthor] = useState("");
    const [bookStatus, setBookStatus] = useState(false);
    const [idSelected, setIdSelected] = useState(null);


  
    // add a new book
    const handleAddBook= () => {
        setBooks([...books, {id: uuidv4(), name: bookName, author: bookAuthor, read: bookStatus}]);
        setBookName("");
        setBookAuthor("");
        setBookStatus(false);
        if (idSelected) toggleStatus(idSelected);
    };

    // update book
    const handleUpdateBook= (id) => {
        const newBooks = books.map(book => {
            if (book.id === id) {
                book.name = bookName;
                book.author = bookAuthor;
                book.read = bookStatus;
            }
            return book;
        });
        setBooks([...newBooks]);
        setBookName("");
        setBookAuthor("");
        setBookStatus(false);
        toggleStatus(id);
    };
  
    // delete book
    const handleDeleteBook = (id) => {
        const newBooks = books.filter(book => book.id !== id);
        setBooks(newBooks);
        setBookName("");
        setBookAuthor("");
        setBookStatus(false);
        setIdSelected(null);
    };

    const handleCheck = () => {
        //const flag = bookStatus;
        setBookStatus(!bookStatus);
    };
  
    // change book status
    const toggleStatus = (id) => {
        if ((idSelected !== null)&&(id !== idSelected)) {
            const resetButton = document.getElementById(idSelected);
            resetButton.value = "not selected";
            resetButton.innerHTML = "not selected";
        }
        const toggleButton = document.getElementById(id);
        if (toggleButton.value === "not selected") {
            toggleButton.value = "selected";
            toggleButton.innerHTML = "selected";
            setIdSelected(id);
            const selectedBook = books.find(book => book.id === id);
            setBookName(selectedBook.name);
            setBookAuthor(selectedBook.author);
            setBookStatus(selectedBook.read);
        }
        else {
            toggleButton.value = "not selected";
            toggleButton.innerHTML = "not selected";
            setIdSelected(null);
            setBookName("");
            setBookAuthor("");
            setBookStatus(false);
        }
    };

    const booksList = books.map(book => (
        <Book key={book.id} book={book} toggleStatus={toggleStatus}/>
    ));

    const history = useHistory();

    const handleLogout = () =>{
        history.replace("/");
    };

    return (
        <div className="library">
            <Banner/>
            <input type="button" className = "btn btn-primary logout-button" value="Logout" onClick={()=>handleLogout()}></input>
            <br></br>
            <br></br>
            <table>
                <tbody>
                    <tr>
                        <td>book name:</td>
                        <td><input type="text" className="bookInput" value={bookName} onChange={event => setBookName(event.target.value)} placeholder="book name"  required="required"/></td>
                    </tr>
                    <tr>
                        <td>author:</td>
                        <td><input type="text" className="bookInput" value={bookAuthor} onChange={event => setBookAuthor(event.target.value)} placeholder="author"/></td>
                    </tr>
                    <tr>
                        <td>book is read</td>
                        <td><label className="switch"><input type="checkbox"  onChange={handleCheck} checked={bookStatus}></input><span className="slider round"></span></label></td>
                    </tr>
                </tbody>
            </table>
            <table>
                <tbody>
                    <tr>
                        <td><input type="button" className = "btn btn-success" value="Add" onClick={() => handleAddBook()}/></td>
                        <td><input type="button" className="btn btn-secondary" value="Update" onClick={() => handleUpdateBook(idSelected)}/></td>
                        <td><input type="button" className="btn btn-danger" value="Delete" onClick={() => handleDeleteBook(idSelected)}/></td>
                    </tr>
                </tbody>
            </table>
            <br></br>
            <h3>Book list:</h3>
            <table>
                <thead>
                    <tr>
                        <th className="bookList">book</th>
                        <th className="bookList">author</th>
                        <th className="bookList">status</th>
                       
                    </tr>
                </thead>
                <tbody>
                    {booksList}
                </tbody>
            </table>
        </div>
    );  
}