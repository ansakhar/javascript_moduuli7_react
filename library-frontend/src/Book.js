import React from "react";

export default function Book({book, toggleStatus}) {
    function handleToggleStatus() {
        toggleStatus(book.id);
    }
    return (
        <tr className="bookRivi">
            <td className="bookName"> {book.name}</td>
            <td className="bookName"> {book.author}</td>
            <td className="bookStatus"><span>{book.read ? "Read": "Not read"}</span></td>
            <td>
                <button className="selected" id={book.id} value="not selected" onClick={() => handleToggleStatus()}>not selected</button>
            </td>
        </tr>
    );
}
  