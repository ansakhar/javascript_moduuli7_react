import './App.css';
import React, { useState, useEffect } from 'react';

function Timer() {

const [second, setSecond] = useState(0);
const [minute, setMinute] = useState(0);
const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    let intervalId;

    if (isActive) {
      intervalId = setInterval(() => {
        addSecond();
      }, 1000)
    }
    return () => clearInterval(intervalId);
  }, [isActive, minute, second])

  function resetTimer() {
    setSecond(0);
    setMinute(0);
  }

  function addMinute() {
    setMinute(minute+1);
  }

  function addSecond() {
    if(second === 59) {
      setSecond(0);
      setMinute(minute+1)
    }
    else {
      setSecond(second+1)
    }
  }

  return (
    <div className="container">
      <div className="time">
        <span>{String(minute).padStart(2,"0")}</span>
        <span>:</span>
        <span>{String(second).padStart(2,"0")}</span>
      </div>
      <div className="buttons">
        <button onClick={() => setIsActive(!isActive)}>
          {isActive ? "Pause": "Start"}
        </button>
        <button onClick={() => addMinute()}>Add minute</button>
        <button onClick={() => addSecond()}>Add second</button>
        <button onClick={() => resetTimer()}>Reset</button>
      </div>
   </div>
  )
}
function App() {
  return (
    <div>
<Timer/>
   </div>
  );
}

export default App;
