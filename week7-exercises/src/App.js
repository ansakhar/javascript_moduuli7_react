import './App.css';
import React, { useState } from 'react';

function Bingo() {

const [numbers, setNumbers] = useState([]);
const [arhiveNumbers, setArhiveNumbers] = useState([]);
const [isOrdered, setIsOrdered] = useState(false);

const randomNumber = () => {
 // e.preventDefault();
  if(numbers.length === 5) {
    alert("Can't add more numbers");
    return};
  const newNumber = Math.floor(Math.random() * 70) + 1;
  //const findNumber = numbers.find(number => number === newNumber)
  if (numbers.includes(newNumber)) randomNumber()
  else addNumber(newNumber);
};

const addNumber = (newNumber) => setNumbers([...numbers, newNumber]);

const orderNumbers = () => {
  setIsOrdered(!isOrdered);
  if (!isOrdered) {
    setArhiveNumbers([...numbers]);
     const orderedNumbers = numbers.sort(function(a, b) {
      return a - b;
    });
    setNumbers(orderedNumbers);
}
else {
  setNumbers(arhiveNumbers);
}
};

const renderNumbers = () => numbers.map((number) => (
  <div className = "bg-warning rounded-circle bingo-number mx-auto" key={number}>
    {number}
    </div>
));

  return (
    <div  className = "m-auto text-center">
      {renderNumbers()}
        <button className = "btn btn-secondary" onClick={() => setNumbers([])}>Reset</button>
        <button className = "btn btn-primary" onClick={randomNumber}>+</button>
        <button className = "btn btn-success" onClick={orderNumbers}>{isOrdered ? "Reorder": "Order"}</button>
   </div>
  )
}
function App() {
  return (
    <div>
<Bingo/>
   </div>
  );
}

export default App;
