import './App.css';
import React, { useState } from "react";

function App() {
  const [inputString, setInputString] = useState("");
  const [visibleString, setVisibleString] = useState("");

  function formSubmitHandler(event) {
    // If preventDefault() is not called, the page will reload after clicking the button
    event.preventDefault();
    setVisibleString(inputString);
  }

  function textChangeHandler(event) {
    setInputString(event.target.value);
  }

  return (
    <>
      <h1>Your string is: {visibleString}</h1>
      <form onSubmit={formSubmitHandler}>
        <input type="text" value={inputString} onChange={textChangeHandler} />
        <button type="submit">Submit</button>
      </form>
    </>
  );
}

export default App;