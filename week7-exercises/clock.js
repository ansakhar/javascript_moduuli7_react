import React, { useState } from "react";
import './App.css';

/* Assignment 3 */
function App() {

  function Clock() {

    const [time, setTime] = useState(new Date());

    setInterval(() => setTime(new Date()), 1000 );

    return (
        <h1 className="Clock">{`${time.toLocaleDateString()} ${time.toLocaleTimeString()}`}</h1>
    );
  };

  return (
    <div className="App">
      <Clock />
    </div>
  );
}

export default App;